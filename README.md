# surcharge-calculator

![](https://img.shields.io/badge/written%20in-Javascript-blue)

Determine what surcharge is worth paying in order to offset a loan.


## Download

- [⬇️ surcharge-calculator.html](dist-archive/surcharge-calculator.html) *(1.88 KiB)*
